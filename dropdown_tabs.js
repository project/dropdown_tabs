Drupal.behaviors.dropdownTabsInitialize = function(context) {
	/* For the tabs submit button */
	$('#dropdown-tabs-form .form-submit').css('display', 'none');
	$('.dropdown-tabs-select-box').change(function(){
		$('#dropdown-tabs-form').submit();		
	});
};